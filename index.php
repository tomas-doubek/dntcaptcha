<?php
   include "autoload.php";
   $captcha = new DntCaptcha();
   $captcha->load();
   ?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="DntCaptcha Demo - Ajax Captcha">
      <meta name="author" content="Tomas Doubek">
      <link rel="icon" href="./media/grf/favicon.ico">
      <title>DntCaptcha Demo - Ajax Captcha</title>
      <!-- Bootstrap core CSS -->
      <link href="./media/css/bootstrap.min.css" rel="stylesheet">
      <link href="./media/css/custom.css" rel="stylesheet">
      <style>

      </style>
   </head>
   <body>
      <script src="./media/js/jquery-1.11.3.min.js"></script>
      <script type="text/javascript">
		$(document).ready(function() {
			$("#registration_form").validate({
				rules: {
					name: {
						required: true,
						minlength: 1
					},

				},
				messages: {
					name: "Please fill your name",
				},

				submitHandler: function(form) {
					$.ajax({
						type: "POST",
						url: "./ajax.php",
						data: $(form).serialize(),
						timeout: "10000",
						dataType: "json",
						success: function(data) {
							//var data = jQuery.parseJSON(data);
							console.log(data);
							if (data.success == 1) {
								//$("#registration_form").css("display", "none");
								$("#form_ok").css("display", "block");
								$("#your_name").html(data.name);
							} else if (data.success == 2) {
								alert("No valid captcha");
							} else if (data.success == 0) {
								alert("...in progress...");
							} else {
								writeError(data.message);
							}
						},
						error: function() {
							alert("Please try again later...");
						}
					});
					return false;
				}


			});
		});
	</script>
      <div class="container">
	  
	  <div class="starter-template">
        <h1>DntCaptcha - Ajax Captcha</h1>
      </div>
	  
         <div class="starter-template">
            <form id="registration_form" action="#" method="POST">
               <div class="form-group">
                  <label for="exampleInputEmail1">Your name</label>
                  <input type="text" name="name" class="form-control"  placeholder="Enter Name">
               </div>
               <?php $captcha->show(); ?>
               <input type="submit" name="sent" class="btn btn-primary" value="Sent" />
            </form>
            <div id="form_ok">
               Ok, valid post data. Your name is <span id="your_name"></span>
            </div>
         </div>
      </div>
      <!-- /.container -->
      <script type='text/javascript' src="./media/js/jquery.validate.js"></script>
   </body>
</html>