<?php
header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
include "autoload.php";

$captcha = new DntCaptcha();

if(isset($_POST['sent'])){
	if($captcha->isCaptchaValid()){
		$RESPONSE = 1;
		$NAME = $_POST['name'];
	}else{
		$RESPONSE = 2;
		$NAME = false;	
	}
}else{
	$RESPONSE = 0;
	$NAME = false;	
}
	
echo '
{
  "success": "'.$RESPONSE.'",
  "name": "'.$NAME.'",
  "request": "POST",
  "response": "'.$RESPONSE.'",
  "generator": "Designdnt 3",
  "service": "dnt-ajax-universal",
  "message": "Silence is golden, speech is gift :)"
}';