<?php
/*
 *
 * Class:		 		Captcha
 * Framework:			Dnt3
 * version:			4.2.9
 * developed by:	 	Tomas Doubek
 * contact:		 	thomas.doubek@gmail.com
 *
 *
*/

class DntCaptcha{
	
	var $rand_num; //rand number
	var $bigger_num; //bigger number
	var $smaller_num; //smaller number
	
	var $show_bigger_num; //show bigger number
	var $show_smaller_num; //show smaller number
	
	
	var $num_diff; //show numeric diff or sum
	var $post; //is post captcha [name="captcha"]
	var $validation; //is validation
	
/*
 * construct 
 * start session after init class
 *
*/
	public function __construct(){
		if(!isset($_SESSION)){
			session_start();
		}
	}
	
	
/*
 * randNum 
 * this function is custom function for generate rand numbers
 *
*/
	protected function randNum($from, $to){
		$this->rand_num = rand($from, $to);
		return $this->rand_num;
	}
	
	
/*
 * setBiggerNumber 
 * this function sets bigger number in skalar diff or sum
 *
*/
	protected function setBiggerNumber(){
		$this->bigger_num = $this->randNum(10, 78);
		$_SESSION['bigger_num'] = $this->bigger_num;
		return $this->bigger_num;
	}
	
	
/*
 * setSmallerNumber 
 * this function sets smaller number in skalar diff or sum
 *
*/
	protected function setSmallerNumber(){
		$this->smaller_num = $this->randNum(1, 9);
		$_SESSION['smaller_num'] = $this->smaller_num;
		return $this->smaller_num;
	}
	
	
/*
 * showSmallerNumber 
 * this function show bigger number to session
 *
*/
	public function showSmallerNumber(){
		$this->show_smaller_num = $_SESSION['smaller_num'];
		return $this->show_smaller_num;
	}
	
	
/*
 * showSmallerNumber 
 * this function show smaller number to session
 *
*/
	public function showBiggerNumber(){
		$this->show_bigger_num = $_SESSION['bigger_num'];
		return $this->show_bigger_num;
	}
	
	
/*
 * showSmallerNumber 
 * this function creat diff or sum of these numbers
 *
*/
	protected function setDiff(){
		$this->num_diff = $this->showBiggerNumber() + $this->showSmallerNumber();
		return $this->num_diff;
	}
	

/* 
 * load
 * this function init functions for generate rand numbers after run captcha
 *
*/
	public function load(){
		$this->setSmallerNumber();
		$this->setBiggerNumber();
	}
	
	
/* 
 * isCaptchaPost
 * this function return TRUE or FALSE if input name="captcha" was sent
 *
*/
	public function isCaptchaPost(){
		
		if(isset($_POST['captcha'])){
			$this->post = $_POST['captcha'];
		}
		else{
			$this->post = false;
		}
		return $this->post;
	}

	
/* 
 * generateImage
 * this function generate image. Input of function is generated number
 *
*/
	public function generateCaptcha($number){
		// Create the image
		$im = imagecreatetruecolor(40, 27);

		// Create some colors
		$white = imagecolorallocate($im, 255, 255, 255);
		$grey 	= imagecolorallocate($im, 128, 128, 128);
		$black = imagecolorallocate($im, 0, 0, 0);
		imagefilledrectangle($im, 0, 0, 399, 29, $white);

		// The text to draw
		$text = $number;
		// Replace path by your own font path
		$font = 'arial.ttf';

		// Add some shadow to the text
		imagettftext($im, 20, 0, 11, 21, $grey, $font, $text);

		// Add the text
		imagettftext($im, 20, 0, 10, 20, $black, $font, $text);

		// Using imagepng() results in clearer text compared with imagejpeg()
		imagepng($im);
		imagedestroy($im);

	}

	
/* 
 * isCaptchaValid
 * this function is part of validation rules where captcha can be valid or invalid
 *
*/	
	public function isCaptchaValid(){
		if($this->isCaptchaPost()){
			if($this->isCaptchaPost() == $this->setDiff()){
				$this->validation = true;
			}else{
				$this->validation = false;
			}
		}else{
			$this->validation = false;
		}
		return $this->validation;
	}
/* 
 * getImage
 * this function return image of generated captcha
 *
*/		
	public function getImage(){
		header("Content-Type: image/png");
		if(isset($_GET['order']) && ($_GET['order'] == 1)){
			$n = $this->showBiggerNumber();
		}
		elseif(isset($_GET['order']) && ($_GET['order'] == 2)){
			$n = $this->showSmallerNumber();
		}
		else{
			$n = 0;
		}
		$this->generateCaptcha($n);
	}
/* 
 * show
 * this function show captcha input with image
 *
*/		
	public function show(){
		echo '
		<div class="dnt-captcha">
			<style type="text/css">/*.dnt-captcha input {width: 25px;height: 25px;margin-bottom: 8px;border: 2px solid #000;border-radius: 3px;}*/
			.dnt-captcha td.mark{font-size: 25px;padding-bottom: 14px;font-weight: bold;}</style>
			<table><tr>
				<td><img src="public/Captcha/captchaImage.php?order=1"></td>
				<td class="mark">+</td>
				<td><img src="public/Captcha/captchaImage.php?order=2"></td> 
				<td class="mark">=</td>
				<td>&nbsp;<input type="text" name="captcha"/></td> 
			</tr></table>
			<br/>
		</div>
		';
	}
}